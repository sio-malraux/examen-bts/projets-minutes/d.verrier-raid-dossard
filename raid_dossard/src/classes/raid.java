package classes;

import java.math.BigDecimal;

/**
 * Definition de la classe raid avec ses attributs
 * raid : permet d'entrer un raid
 * @author Dorian
 *
 */

public class raid {
	
	private String code;
	private String nom;
	private String date_debut;
	private String ville;
	private String region;
	private int nb_maxi_par_equipe;
	private BigDecimal montant_inscription;
	private int nb_femmes;
	private int duree_maxi;
	private int age_minimum;
	
	/**
	 * permet de lire un code
	 * @return String representing code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * permet d'écrire un code
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * permet de lire un nom
	 * @return String representing nom
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * permet d'écrire un nom
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * permet de lire une date de début
	 * @return String representing date_debut
	 */
	public String getDate_debut() {
		return date_debut;
	}
	
	/**
	 * permet d'écrire une date de début
	 * @param date_debut
	 */
	public void setDate_debut(String date_debut) {
		this.date_debut = date_debut;
	}
	
	/**
	 * permet de lire une ville
	 * @return String representing ville
	 */
	public String getVille() {
		return ville;
	}
	
	/**
	 * permet d'écrire une ville
	 * @param ville
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}
	
	/**
	 * permet de lire une région
	 * @return String representing region
	 */
	public String getRegion() {
		return region;
	}
	
	/**
	 * permet d'écrire une region
	 * @param region
	 */
	public void setRegion(String region) {
		this.region = region;
	}
	
	/**
	 * permet de lire un nombre maximum de coureur par équipe
	 * @return Integer representing nb_maxi_par_equipe
	 */
	public int getNb_maxi_par_equipe() {
		return nb_maxi_par_equipe;
	}
	
	/**
	 * permet d'écrire un nombre maximum de coureur par équipe
	 * @param nb_maxi_par_equipe
	 */
	public void setNb_maxi_par_equipe(int nb_maxi_par_equipe) {
		this.nb_maxi_par_equipe = nb_maxi_par_equipe;
	}
	
	/**
	 * permet de lire un montant d'inscription
	 * @return BigDecimal representing montant_inscription
	 */
	public BigDecimal getMontant_inscription() {
		return montant_inscription;
	}
	
	/**
	 * permet d'écrire un montant d'inscription
	 * @param montant_inscription
	 */
	public void setMontant_inscription(BigDecimal montant_inscription) {
		this.montant_inscription = montant_inscription;
	}
	
	/**
	 * permet de lire un nombre de femmes
	 * @return Integer representing nb_femmes
	 */
	public int getNb_femmes() {
		return nb_femmes;
	}
	
	/**
	 * permet d'écrire un nombre de femmes
	 * @param nb_femmes
	 */
	public void setNb_femmes(int nb_femmes) {
		this.nb_femmes = nb_femmes;
	}
	
	/**
	 * permet de lire une durée maximum
	 * @return Integer representing duree_maxi
	 */
	public int getDuree_maxi() {
		return duree_maxi;
	}
	
	/**
	 * permet d'écrire une durée maximum
	 * @param duree_maxi
	 */
	public void setDuree_maxi(int duree_maxi) {
		this.duree_maxi = duree_maxi;
	}
	
	/**
	 * permet de lire un age maximum
	 * @return Integer representing age_minimum
	 */
	public int getAge_minimum() {
		return age_minimum;
	}
	
	/**
	 * permet d'écrire une age minimum
	 * @param age_minimum
	 */
	public void setAge_minimum(int age_minimum) {
		this.age_minimum = age_minimum;
	}
	
	/**
	 * Constructeur par défaut (permet de mettre des valeurs par défaut)
	 */
	public raid() {
		super();
		code = "";
		nom = "";
		date_debut = "";
		ville = "";
		region = "";
		nb_maxi_par_equipe = 0;
		montant_inscription = new BigDecimal(0);
		nb_femmes = 0;
		duree_maxi = 0;
		age_minimum = 0;
	}
	
	/**
	 * Constructeur avec différents paramètres
	 * permet d'associés un code à code
	 * permet d'associés un nom à nom
	 * permet d'associés une date de debut à date_debut
	 * permet d'associés une ville à ville
	 * permet d'associés une region à region
	 * permet d'associés un nombre maximum de coureur par equipe à nb_maxi_par_equipe
	 * permet d'associés un montant d'inscription à montant_inscription
	 * permet d'associés un nombre de femmes à nb_femmes
	 * permet d'associés une duree maximum à duree_maxi
	 * permet d'associés un age minimum à age_minimum
	 * @param code
	 * @param nom
	 * @param date_debut
	 * @param ville
	 * @param region
	 * @param nb_maxi_par_equipe
	 * @param montant_inscription
	 * @param nb_femmes
	 * @param duree_maxi
	 * @param age_minimum
	 */
	public raid(String code, String nom, String date_debut, String ville, String region, int nb_maxi_par_equipe,
			BigDecimal montant_inscription, int nb_femmes, int duree_maxi, int age_minimum) {
		super();
		this.code = code;
		this.nom = nom;
		this.date_debut = date_debut;
		this.ville = ville;
		this.region = region;
		this.nb_maxi_par_equipe = nb_maxi_par_equipe;
		this.montant_inscription = montant_inscription;
		this.nb_femmes = nb_femmes;
		this.duree_maxi = duree_maxi;
		this.age_minimum = age_minimum;
	}
	
	/**
	 * Permet l'affichage de tous les attributs
	 */
	@Override
	public String toString() {
		return "raid [code=" + code + ", nom=" + nom + ", date_debut=" + date_debut + ", ville=" + ville + ", region="
				+ region + ", nb_maxi_par_equipe=" + nb_maxi_par_equipe + ", montant_inscription=" + montant_inscription
				+ ", nb_femmes=" + nb_femmes + ", duree_maxi=" + duree_maxi + ", age_minimum=" + age_minimum + "]";
	}
}