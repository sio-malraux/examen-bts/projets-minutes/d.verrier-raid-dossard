package classes;

/**
 * Definition de la classe activite avec ses attributs
 * activite : permet d'entrer une activité
 * @author Dorian
 *
 */

public class activite {
	
	private String code;
	private String libelle;
	
	/**
	 * permet de lire un code
	 * @return String representing code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * permet d'écrire un code
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * permet de lire un libelle
	 * @return String representing libelle
	 */
	public String getLibelle() {
		return libelle;
	}
	
	/**
	 * permet d'écrire un libelle
	 * @param libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	/**
	 * Constructeur par défaut (permet de mettre des valeurs par défaut)
	 */
	public activite() {
		super();
		code = "";
		libelle = "";
	}
	
	/**
	 * Constructeur avec différents paramètres
	 * permet d'associés un code à code
	 * permet d'associés un libelle à libelle
	 * @param code
	 * @param libelle
	 */
	public activite(String code, String libelle) {
		super();
		this.code = code;
		this.libelle = libelle;
	}

	/**
	 * Permet l'affichage de tous les attributs
	 */
	@Override
	public String toString() {
		return "activite [code=" + code + ", libelle=" + libelle + "]";
	}
}