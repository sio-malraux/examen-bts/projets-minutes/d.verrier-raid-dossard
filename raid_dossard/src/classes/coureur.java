package classes;

import java.sql.Date;

/**
 * Definition de la classe coureur avec ses attributs
 * coureur : permet d'entrer un coureur
 * @author Dorian
 *
 */

public class coureur {
	
	private String licence;
	private String nom;
	private String prenom;
	private String sexe;
	private String nationalite;
	private String mail;
	private boolean certif_med_aptitude;
	private Date date_naissance;
	
	/**
	 * permet de lire une licence
	 * @return String representing licence
	 */
	public String getLicence() {
		return licence;
	}
	
	/**
	 * permet d'écrire une licence
	 * @param licence
	 */
	public void setLicence(String licence) {
		this.licence = licence;
	}
	
	/**
	 * permet de lire un nom
	 * @return String representing nom
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * permet d'écrire un nom
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * permet de lire un prenom
	 * @return String representing prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	
	/**
	 * permet d'écrire un prenom
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	/**
	 * permet de lire un sexe
	 * @return String representing sexe
	 */
	public String getSexe() {
		return sexe;
	}
	
	/**
	 * permet d'écrire un sexe
	 * @param sexe
	 */
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	
	/**
	 * permet de lire une nationalite
	 * @return String representing nationalite
	 */
	public String getNationalite() {
		return nationalite;
	}
	
	/**
	 * permet d'écrire une nationalite
	 * @param nationalite
	 */
	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}
	
	/**
	 * permet de lire un mail
	 * @return String representing mail
	 */
	public String getMail() {
		return mail;
	}
	
	/**
	 * permet d'écrire un mail
	 * @param mail
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	/**
	 * permet de lire une certification medicale d'aptitude
	 * @return Boolean representing certif_med_aptitude
	 */
	public boolean isCertif_med_aptitude() {
		return certif_med_aptitude;
	}
	
	/**
	 * permet d'écrire une certification medicale d'aptitude
	 * @param certif_med_aptitude
	 */
	public void setCertif_med_aptitude(boolean certif_med_aptitude) {
		this.certif_med_aptitude = certif_med_aptitude;
	}
	
	/**
	 * permet de lire une date de naissance
	 * @return Date representing date_naissance
	 */
	public Date getDate_naissance() {
		return date_naissance;
	}
	
	/**
	 * permet d'écrire une date de naissance
	 * @param date_naissance
	 */
	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}
	
	/**
	 * Constructeur par défaut (permet de mettre des valeurs par défaut)
	 */
	public coureur() {
		super();
		licence = "";
		nom = "";
		prenom = "";
		sexe = "";
		nationalite = "";
		mail = "";
		certif_med_aptitude = false;
		date_naissance = new java.sql.Date(2000-01-01);
	}
	
	/**
	 * Constructeur avec différents paramètres
	 * permet d'associés une licence à licence
	 * permet d'associés un nom à nom
	 * permet d'associés un prenom à prenom
	 * permet d'associés un sexe à sexe
	 * permet d'associés une nationalite à nationalite
	 * permet d'associés un mail à mail
	 * permet d'associés une certification medicale d'aptitude à certif_med_aptitude
	 * permet d'associés une date de naissance à date_naissance
	 * @param licence
	 * @param nom
	 * @param prenom
	 * @param sexe
	 * @param nationalite
	 * @param mail
	 * @param certif_med_aptitude
	 * @param date_naissance
	 */
	public coureur(String licence, String nom, String prenom, String sexe, String nationalite, String mail,
			boolean certif_med_aptitude, Date date_naissance) {
		super();
		this.licence = licence;
		this.nom = nom;
		this.prenom = prenom;
		this.sexe = sexe;
		this.nationalite = nationalite;
		this.mail = mail;
		this.certif_med_aptitude = certif_med_aptitude;
		this.date_naissance = date_naissance;
	}

	/**
	 * Permet l'affichage de tous les attributs
	 */
	@Override
	public String toString() {
		return "coureur [licence=" + licence + ", nom=" + nom + ", prenom=" + prenom + ", sexe=" + sexe
				+ ", nationalite=" + nationalite + ", mail=" + mail + ", certif_med_aptitude=" + certif_med_aptitude
				+ ", date_naissance=" + date_naissance + "]";
	}
}