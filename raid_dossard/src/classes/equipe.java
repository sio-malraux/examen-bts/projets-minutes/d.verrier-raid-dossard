package classes;

import java.sql.Date;

/**
 * Definition de la classe equipe avec ses attributs
 * equipe : permet d'entrer une equipe
 * @author Dorian
 *
 */

public class equipe {
	
	private int numero;
	private String code_raid;
	private String nom;
	private Date date_inscription;
	private int temps_global;
	private int classement;
	private int penalites_bonif;
	
	/**
	 * permet de lire un numero
	 * @return Integer representing numero
	 */
	public int getNumero() {
		return numero;
	}
	
	/**
	 * permet d'écrire un numero
	 * @param numero
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	/**
	 * permet de lire un code raid
	 * @return String representing code_raid
	 */
	public String getCode_raid() {
		return code_raid;
	}
	
	/**
	 * permet d'écrire un code raid
	 * @param code_raid
	 */
	public void setCode_raid(String code_raid) {
		this.code_raid = code_raid;
	}
	
	/**
	 * permet de lire un nom
	 * @return String representing nom
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * permet d'écrire un nom
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * permet de lire une date d'inscription
	 * @return Date representing date_inscription
	 */
	public Date getDate_inscription() {
		return date_inscription;
	}
	
	/**
	 * permet d'écrire une date d'inscription
	 * @param date_inscription
	 */
	public void setDate_inscription(Date date_inscription) {
		this.date_inscription = date_inscription;
	}
	
	/**
	 * permet de lire un temps global
	 * @return Integer representing temps_global
	 */
	public int getTemps_global() {
		return temps_global;
	}
	
	/**
	 * permet d'écrire un temps global
	 * @param temps_global
	 */
	public void setTemps_global(int temps_global) {
		this.temps_global = temps_global;
	}
	
	/**
	 * permet de lire un classement
	 * @return Integer representing classement
	 */
	public int getClassement() {
		return classement;
	}
	
	/**
	 * permet d'écrire un classement
	 * @param classement
	 */
	public void setClassement(int classement) {
		this.classement = classement;
	}
	
	/**
	 * permet de lire une penalite bonif
	 * @return Integer representing penalites_bonif
	 */
	public int getPenalites_bonif() {
		return penalites_bonif;
	}
	
	/**
	 * permet d'écrire une penalite bonif
	 * @param penalites_bonif
	 */
	public void setPenalites_bonif(int penalites_bonif) {
		this.penalites_bonif = penalites_bonif;
	}
	
	/**
	 * Constructeur par défaut (permet de mettre des valeurs par défaut)
	 */
	public equipe() {
		super();
		numero = 0;
		code_raid = "";
		nom = "";
		date_inscription = new java.sql.Date(2020-06-15);
		temps_global = 0;
		classement = 0;
		penalites_bonif = 0;
	}
	
	/**
	 * Constructeur avec différents paramètres
	 * permet d'associés un numero à numero
	 * permet d'associés un code raid à code_raid
	 * permet d'associés un nom à nom
	 * permet d'associés une date d'inscription à date_inscription
	 * permet d'associés un temps global à temps_global
	 * permet d'associés un classement à classement
	 * permet d'associés une penalite bonif à penalites_bonif
	 * @param numero
	 * @param code_raid
	 * @param nom
	 * @param date_inscription
	 * @param temps_global
	 * @param classement
	 * @param penalites_bonif
	 */
	public equipe(int numero, String code_raid, String nom, Date date_inscription, int temps_global, int classement,
			int penalites_bonif) {
		super();
		this.numero = numero;
		this.code_raid = code_raid;
		this.nom = nom;
		this.date_inscription = date_inscription;
		this.temps_global = temps_global;
		this.classement = classement;
		this.penalites_bonif = penalites_bonif;
	}

	/**
	 * Permet l'affichage de tous les attributs
	 */
	@Override
	public String toString() {
		return "equipe [numero=" + numero + ", code_raid=" + code_raid + ", nom=" + nom + ", date_inscription="
				+ date_inscription + ", temps_global=" + temps_global + ", classement=" + classement
				+ ", penalites_bonif=" + penalites_bonif + "]";
	}
}