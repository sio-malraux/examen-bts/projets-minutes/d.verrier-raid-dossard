package classes;

/**
 * Definition de la classe integrer_equipe avec ses attributs
 * integrer_equipe : permet au coureur d integrer une équipe
 * @author Dorian
 *
 */

public class integrer_equipe {
	
	private int numero_equipe;
	private String code_raid;
	private String licence;
	private int temps_individuel;
	private String num_dossard;
	
	/**
	 * permet de lire un numero d'équipe
	 * @return Integer representing numero_equipe
	 */
	public int getNumero_equipe() {
		return numero_equipe;
	}
	
	/**
	 * permet d'écrire un numero d'équipe
	 * @param numero_equipe
	 */
	public void setNumero_equipe(int numero_equipe) {
		this.numero_equipe = numero_equipe;
	}
	
	/**
	 * permet de lire un code raid
	 * @return String representing code_raid
	 */
	public String getCode_raid() {
		return code_raid;
	}
	
	/**
	 * permet d'écrire un code raid
	 * @param code_raid
	 */
	public void setCode_raid(String code_raid) {
		this.code_raid = code_raid;
	}
	
	/**
	 * permet de lire une licence
	 * @return String representing licence
	 */
	public String getLicence() {
		return licence;
	}
	
	/**
	 * permet d'écrire une licence
	 * @param licence
	 */
	public void setLicence(String licence) {
		this.licence = licence;
	}
	
	/**
	 * permet de lire un temps individuel
	 * @return Integer representing temps_individuel
	 */
	public int getTemps_individuel() {
		return temps_individuel;
	}
	
	/**
	 * permet d'écrire un temps individuel
	 * @param temps_individuel
	 */
	public void setTemps_individuel(int temps_individuel) {
		this.temps_individuel = temps_individuel;
	}
	
	/**
	 * permet de lire un numero de dossard
	 * @return String representing num_dossard
	 */
	public String getNum_dossard() {
		return num_dossard;
	}
	
	/**
	 * permet d'écrire un numero de dossard
	 * @param num_dossard
	 */
	public void setNum_dossard(String num_dossard) {
		this.num_dossard = num_dossard;
	}
	
/**
 * Constructeur par défaut (permet de mettre des valeurs par défaut)
 */
	public integrer_equipe() {
		super();
		numero_equipe = 0;
		code_raid = "";
		licence = "";
		temps_individuel = 0;
		num_dossard = "";
	}
	
	/**
	 * Constructeur avec différents paramètres
	 * permet d'associés un numero d'équipe à numero_equipe
	 * permet d'associés un code raid à code_raid
	 * permet d'associés une licence à licence
	 * permet d'associés un temps individuel à temps_individuel
	 * permet d'associés un numero de dossard à num_dossard
	 * @param numero_equipe
	 * @param code_raid
	 * @param licence
	 * @param temps_individuel
	 * @param num_dossard
	 */
	public integrer_equipe(int numero_equipe, String code_raid, String licence, int temps_individuel,
			String num_dossard) {
		
		this.numero_equipe = numero_equipe;
		this.code_raid = code_raid;
		this.licence = licence;
		this.temps_individuel = temps_individuel;
		this.num_dossard = num_dossard;
	}

	/**
	 * Permet l'affichage de tous les attributs
	 */
	@Override
	public String toString() {
		return "integrer_equipe [numero_equipe=" + numero_equipe + ", code_raid=" + code_raid + ", licence=" + licence
				+ ", temps_individuel=" + temps_individuel + ", num_dossard=" + num_dossard + "]";
	}
}