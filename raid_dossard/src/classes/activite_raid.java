package classes;

/**
 * Definition de la classe activite_raid avec ses attributs
 * activite_raid : permet d'entrer une activité pour des raids
 * @author Dorian
 *
 */

public class activite_raid {
	
	private String code_raid;
	private String code_activite;
	
	/**
	 * permet de lire un code raid
	 * @return String representing code_raid
	 */
	public String getCode_raid() {
		return code_raid;
	}
	
	/**
	 * permet d'écrire un code raid
	 * @param code_raid
	 */
	public void setCode_raid(String code_raid) {
		this.code_raid = code_raid;
	}
	
	/**
	 * permet de lire un code d'activité
	 * @return String representing code_activite
	 */
	public String getCode_activite() {
		return code_activite;
	}
	
	/**
	 * permet d'écrire un code d'activité
	 * @param code_activite
	 */
	public void setCode_activite(String code_activite) {
		this.code_activite = code_activite;
	}
	
	/**
	 * Constructeur par défaut (permet de mettre des valeurs par défaut)
	 */
	public activite_raid() {
		super();
		code_raid = "";
		code_activite = "";
	}
	
	/**
	 * Constructeur avec différents paramètres
	 * permet d'associés un code raid à code_raid
	 * permet d'associés un code d'activité à code_activite
	 * @param code_raid
	 * @param code_activite
	 */
	public activite_raid(String code_raid, String code_activite) {
		super();
		this.code_raid = code_raid;
		this.code_activite = code_activite;
	}
	
	/**
	 * Permet l'affichage de tous les attributs
	 */
	@Override
	public String toString() {
		return "activite_raid [code_raid=" + code_raid + ", code_activite=" + code_activite + "]";
	}
}