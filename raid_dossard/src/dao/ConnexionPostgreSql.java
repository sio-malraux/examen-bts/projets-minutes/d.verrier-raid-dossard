package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionPostgreSql {
	
	private static String url = "jdbc:postgresql://postgresql.bts-malraux72.net:5432/d.verrier";
	private static String user = "d.verrier";
	private static String password = "P@ssword";
	private static Connection connect = null;
	
	public static Connection getInstance() {
		
		if (connect == null) {
			
			try {
				connect = DriverManager.getConnection(url, user, password);
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		return connect;
	}
	
	public static void Deconnect() {
		
		try {
			connect.close();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}