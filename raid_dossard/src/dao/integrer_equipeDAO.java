package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class integrer_equipeDAO {

	public Connection connect = ConnexionPostgreSql.getInstance();
	
	public void num_dossard(String code) {
		
			try {
		          ResultSet result = this.connect
		                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
		                                                ResultSet.CONCUR_UPDATABLE)
		                             .executeQuery("select * from \"raid_dossard\".raid "
		                             		+ "WHERE code = '" + code +"'");
		          
		          if(result.first()) {
		        	  
		        	  int num_equipe = 0;
		        	  String licence = "";
		        	  
		        	  try {
				          result = this.connect
				                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
				                                                ResultSet.CONCUR_UPDATABLE)
				                             .executeQuery("select * from \"raid_dossard\".integrer_equipe "
				                             		+ "WHERE code_raid = '" + code +"'");
				          
				          if(result.first()) {
				        	  
				        	  num_equipe = result.getInt("numero_equipe");
				        	  licence = result.getString("licence");
				        	  String numero_equipe = String.valueOf(num_equipe);
				        	  String prenom = "";
				        	  String nom = "";
				        	  
				        	  try {
				      			Statement requete = this.connect.createStatement();

				      			ResultSet curseur = requete.executeQuery(" SELECT * \n" + 
				      					"					FROM raid_dossard.integrer_equipe ie  \n" + 
				      					"					INNER JOIN raid_dossard.coureur c ON ie.licence = c.licence  \n" +  
				      					"					WHERE code_raid = '"+code+"'");  
				      			
				      			int c = 1;

				      			while (curseur.next()){
				      				
				      				prenom = result.getString("prenom");
				      				nom = result.getString("nom");
				      				String p = prenom.substring(0, 1).toUpperCase();
				      				String n = nom.substring(0, 1).toUpperCase();
				      				String compteur = String.valueOf(c);
				      				
				      				String num_dossard = numero_equipe + "-" + code + "-" + p + n + "-" + compteur;
				      				
				      				try { this .connect	
				      		            .createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
				      		         		             ResultSet.CONCUR_UPDATABLE )
				      		            .executeUpdate("UPDATE \"raid_dossard\".integrer_equipe SET num_dossard = '" + num_dossard + "'" +
				      		                 	      " WHERE numero_equipe = '" + num_equipe + "', code_raid = '" + code + "', licence = '" + licence + "'" );
				      					
				      				}
				      				catch (SQLException e) {
				      			      	e.printStackTrace();
				      				}
				      				
				      				c++;
				      			}				      					
				      			curseur.close();
				      			requete.close();
				      		} catch (SQLException e) {
				      			e.printStackTrace();
				      		} 				        	  
				          }
				    }
					catch (SQLException e) {
						        e.printStackTrace();
					}
		          }
		    }
			catch (SQLException e) {
				        e.printStackTrace();
			}
	}
}